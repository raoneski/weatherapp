# Weather App

https://developer.yahoo.com/weather/documentation.html?guccounter=1

## Design Pattern

**Android, Kotlin, MVP, Koin, Coroutines, AndroidX, Jetpack Compose, Retrofit2**

## Screens

![photo_1](imgs/photo_1.jpg)![photo_2](imgs/photo_2.jpg)

![photo_3](imgs/photo_3.jpg)![photo_4](imgs/photo_4.jpg)

![photo_5](imgs/photo_5.jpg)
