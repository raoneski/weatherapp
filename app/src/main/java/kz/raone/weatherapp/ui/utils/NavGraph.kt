package kz.raone.weatherapp.ui.utils

import android.os.Parcelable
import androidx.compose.runtime.Immutable
import kotlinx.android.parcel.Parcelize
import kz.raone.weatherapp.api.pojo.Forecast

sealed class Destination : Parcelable {

    @Parcelize
    object Home : Destination()

    @Immutable
    @Parcelize
    data class DayDetail(val index: Int) : Destination() {
    }
}

class Actions(navigator: Navigator<Destination>) {

    val dayDetail: (index: Int) -> Unit = {

        navigator.navigate(Destination.DayDetail(it))
    }

    val home: () -> Unit = {
        navigator.navigate(Destination.Home)
    }

    val pressOnBack: () -> Unit = {
        navigator.back()
    }
}