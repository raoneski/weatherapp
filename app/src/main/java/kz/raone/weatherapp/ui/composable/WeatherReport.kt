package kz.raone.weatherapp.ui.composable

import android.util.Log
import androidx.activity.OnBackPressedDispatcher
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.animation.Crossfade
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.lazy.LazyRowForIndexed
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.runtime.*
import androidx.compose.runtime.savedinstancestate.rememberSavedInstanceState
import androidx.compose.ui.*
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.ExperimentalFocus
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.FocusState
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.imageResource
import androidx.compose.ui.res.loadVectorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kz.raone.weatherapp.R
import kz.raone.weatherapp.api.pojo.Forecast
import kz.raone.weatherapp.api.pojo.SearchItem
import kz.raone.weatherapp.api.pojo.YahooWeather
import kz.raone.weatherapp.manager.SettingsManager
import kz.raone.weatherapp.presenter.YahooPresenter
import kz.raone.weatherapp.ui.utils.*
import kz.raone.weatherapp.ui.values.*
import org.koin.android.ext.android.get
import org.koin.core.context.GlobalContext

@ExperimentalFocus
@Composable
fun WeatherReport(
    weatherState: MutableState<YahooWeather>,
    backDispatcher: OnBackPressedDispatcher,
    searchItems: MutableState<List<SearchItem>>,
    yahooPresenter: YahooPresenter,
    progressBarState: MutableState<Boolean>) {

    val settingsManager: SettingsManager = GlobalContext.get().koin.get ()
    val tabStateToday = mutableStateOf(true)
    val tabStateTomorrow = mutableStateOf(false)
    val tabStateNextWeek = mutableStateOf(false)
    val searchViewState = mutableStateOf(false)

    val navigator: Navigator<Destination> = rememberSavedInstanceState(
        saver = Navigator.saver(backDispatcher)
    ) {
        Navigator(Destination.Home, backDispatcher)
    }
    val actions = remember(navigator) { Actions(navigator) }

    Box(
        modifier = Modifier
            .background(Color.White)
            .fillMaxSize()
    ) {
        val icMenu = loadVectorResource(id = R.drawable.ic_menu)
        val icSearch = loadVectorResource(id = R.drawable.ic_search)
        val searchTextState = remember { mutableStateOf(TextFieldValue()) }

        Column() {
            TopAppBar(
                title = {
                    Box {
                        if (!searchViewState.value) {
                            Text(
                                text = if (weatherState.value.location != null) "${weatherState.value.location?.city}, ${weatherState.value.location?.country}" else stringResource(id = R.string.app_name),
                                modifier = Modifier,
                                fontSize = 16.sp,
                                fontWeight = FontWeight.Bold
                            )
                        } else {
                            AutoFocusingText(searchTextState)
                        }
                    }
                },
                navigationIcon = {
                    IconButton(onClick = { }) {
                        icMenu.resource.resource?.let {
                            Icon(
                                asset = it,
                                modifier = Modifier
                                    .fillMaxWidth()
                            )
                        }
                    }
                },
                backgroundColor = Color.White,
                actions = {
                    FloatingActionButton(
                        onClick = {
                            searchViewState.value = !searchViewState.value

                            if (searchTextState.value.text.isNotEmpty()) {
                                yahooPresenter.getYahooWeather(searchTextState.value.text.toLowerCase())
                                searchTextState.value = TextFieldValue()
                            }
                        },
                        modifier = Modifier.size(42.dp),
                        elevation = 0.dp,
                        backgroundColor = Color.White,
                        contentColor = mainTextColor
                    ) {
                        if (!searchViewState.value || searchTextState.value.text.isNotEmpty()) {
                            icSearch.resource.resource?.let {
                                Icon(
                                    asset = it,
                                    modifier = Modifier
                                        .fillMaxWidth()
                                )
                            }
                        } else {
                            Icon(Icons.Filled.Clear)
                        }
                    }
                },
                elevation = 0.dp
            )

            Providers(BackDispatcherAmbient provides backDispatcher) {
                ProvideDisplayInsets {
                    Crossfade(navigator.current) { destination ->
                        when (destination) {
                            is Destination.Home -> {
                                ScrollableColumn() {

                                    Row(
                                        modifier = Modifier.height(60.dp).align(Alignment.CenterHorizontally),
                                    ) {
                                        ItemTabBar("Today", tabStateToday)
                                        ItemTabBar("Tomorrow", tabStateTomorrow)
                                        ItemTabBar("Next Week", tabStateNextWeek)
                                    }

                                    BodyTabBar(weatherState)

                                    Text(
                                        text = "Next ${weatherState.value.forecasts.size} Days",
                                        modifier = Modifier
                                            .padding(start = 24.dp, top = 12.dp, end = 12.dp, bottom = 12.dp)
                                            .background(Color.White)
                                            .align(Alignment.Start),
                                        color = secondaryTextColor,
                                        style = MaterialTheme.typography.h3,
                                        fontSize = 18.sp
                                    )

                                    LazyRowForIndexed(
                                        items = weatherState.value.forecasts,
                                    ) { index, item ->
                                        MiniBody(index, item, clickableModifier = Modifier.clickable(onClick = {
                                            actions.dayDetail(index)
                                        }) )
                                    }

                                    Row(
                                        modifier = Modifier.align(Alignment.CenterHorizontally)
                                            .padding(top = 24.dp, bottom = 30.dp)
                                    ) {
                                        Card(
                                            modifier = Modifier
                                                .size(10.dp),
                                            shape = CircleShape,
                                            backgroundColor = purple200,
                                            elevation = 0.dp
                                        ) {

                                        }

                                        Spacer(modifier = Modifier.width(10.dp))
                                        Card(
                                            modifier = Modifier
                                                .size(10.dp),
                                            shape = CircleShape,
                                            backgroundColor = purple50,
                                            elevation = 0.dp
                                        ) {

                                        }

                                        Spacer(modifier = Modifier.width(10.dp))
                                        Card(
                                            modifier = Modifier
                                                .size(10.dp),
                                            shape = CircleShape,
                                            backgroundColor = purple50,
                                            elevation = 0.dp
                                        ) {

                                        }

                                    }

                                }

                            }
                            is Destination.DayDetail -> {
                                Box(
                                    modifier = Modifier.fillMaxSize()
                                ) {

                                    MiniBody(
                                        destination.index,
                                        weatherState.value.forecasts[destination.index],
                                        Modifier
                                            .padding(top = 40.dp)
                                            .fillMaxSize(),
                                        shape = RoundedCornerShape(topLeft = 32.dp, topRight = 32.dp),
                                        textSizeTitle = 46.sp,
                                        textSizeLowHigh = 40.sp,
                                        textSizeLow = 31.sp,
                                        topSpacerHeight = 48.dp,
                                        imageSize = 96.dp,
                                        showInfo = true,
                                        elevation = 0.dp
                                    )

                                    FloatingActionButton(
                                        onClick = {
                                            actions.home()
                                        },
                                        backgroundColor = Color.White,
                                        contentColor = colorClearRed,
                                        modifier = Modifier
                                            .padding(top = 12.dp)
                                            .align(Alignment.TopCenter),
                                        elevation = 9.dp
                                    ) {
                                        Icon(Icons.Filled.Clear)
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }


        if (searchViewState.value) {
            Card(
                elevation = 5.dp,
                backgroundColor = Color.White,
                modifier = Modifier.padding(top = 64.dp)
            ) {
                Column {
                    Spacer(modifier = Modifier.height(12.dp))
                    Text(
                        text = "Enter the request text in the format {city},{coutnryCode}, For example: (\"almaty,kz\", \"astana,kz\", \"sunnyvale,ca\")\n\nOr choose from the list.",
                        modifier = Modifier.padding(start = 16.dp, end = 16.dp),
                        color = secondaryTextColor,
                        fontSize = 13.sp
                    )

                    Spacer(modifier = Modifier.height(12.dp))
                    ScrollableColumn(
                        modifier = Modifier
                            .preferredHeight(200.dp)
                    ) {
                        searchItems.value.forEach {
                            Box(
                                modifier = Modifier
                                    .preferredHeight(42.dp)
                                    .fillMaxWidth()
                                    .clickable(onClick = {
                                        GlobalScope.launch {
                                            settingsManager.saveString(SettingsManager.SEARCH_ITEM, Gson().toJson(it))
                                        }

                                        searchViewState.value = !searchViewState.value
                                    }),
                            ) {
                                Text(
                                    text = "${it.city}, ${it.country}",
                                    modifier = Modifier.align(Alignment.CenterStart).padding(start = 16.dp),
                                    fontSize = 14.sp,
                                    fontWeight = FontWeight.W700,
                                    color = mainTextColor,

                                )
                            }

                        }
                    }
                }
            }
        }

        if (progressBarState.value) {
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .background(Color(0x88000000))
            ) {

            }

            Box(
                modifier = Modifier
                    .clip(RoundedCornerShape(6.dp))
                    .background(Color.White)
                    .align(Alignment.Center)
                    .width(200.dp)
                    .height(100.dp)
            ) {
                CircularProgressIndicator(
                    modifier = Modifier.align(Alignment.Center)
                )
            }
        }
    }
}

@ExperimentalFocus
@Composable
fun AutoFocusingText(textState: MutableState<TextFieldValue>) {
    val focusState = remember { mutableStateOf(FocusState.Inactive) }
    val focusRequester = FocusRequester()
    val focusModifier = Modifier.focus()
        val focusRequesterModifier = Modifier.focusRequester(focusRequester)
        OutlinedTextField(
            modifier = focusModifier.then(focusRequesterModifier),
            value = textState.value,
            onValueChange = { value: TextFieldValue ->
                textState.value = value
            },
            keyboardType = KeyboardType.Text,
            imeAction = ImeAction.Search,
            textStyle = MaterialTheme.typography.body2
        )
    onActive {
        focusRequester.requestFocus()
    }
}

@Composable
fun ItemTabBar(title: String, state: MutableState<Boolean>) {
    FloatingActionButton(
        onClick = {},
        backgroundColor = if (state.value) tabBgActiveColor else Color.White,
        modifier = Modifier
            .padding(start = 4.dp, top = 12.dp, end = 4.dp, bottom = 12.dp)
            .height(40.dp),
        elevation = 0.dp
    ) {
        Text(
            text = title,
            color = if (state.value) tabTextActiveColor else mainTextColor,
            modifier = Modifier.padding(start = 16.dp, end = 16.dp),
            fontWeight = FontWeight.Bold,
            fontSize = 14.sp
        )
    }
}

@Composable
fun BodyTabBar(weatherState: MutableState<YahooWeather>) {
    val icSunCould = loadVectorResource(id = R.drawable.ic_sun_cloud)
    val lighticons2 = imageResource(id = R.drawable.lighticons_2)
    Box(
        modifier = Modifier.height(280.dp)
    ) {

        val imageModifier = Modifier
            .fillMaxSize()
            .clip(shape = RoundedCornerShape(26.dp))

        Box(
            modifier = Modifier
                .padding(start = 16.dp, end = 16.dp, bottom = 12.dp)
                .fillMaxWidth()
                .align(Alignment.TopCenter)
                .clip(RoundedCornerShape(26.dp))
                .background(purple200)
        ) {

            Image(
                lighticons2,
                modifier = imageModifier,
                contentScale = ContentScale.FillHeight,
            alpha = 0.3f)

            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier.align(Alignment.TopCenter)
            ) {
                Spacer(modifier = Modifier.height(1.dp))
                Text(
                    text = "${if (weatherState.value.currentObservation != null) weatherState.value.currentObservation?.condition?.temperature else 0}°",
                    fontWeight = FontWeight.Medium,
                    fontSize = 85.sp,
                    color = Color.White,
                    modifier = Modifier.align(Alignment.CenterHorizontally)
                )
                Text(
                    text = "${if (weatherState.value.currentObservation != null) weatherState.value.currentObservation?.condition?.text else 0}",
                    fontWeight = FontWeight.W400,
                    fontSize = 24.sp,
                    color = Color.White
                )

                Spacer(modifier = Modifier.height(16.dp))

                Text(
                    text = "Humidity",
                    fontWeight = FontWeight.W700,
                    fontSize = 19.sp,
                    color = Color.White
                )
                Text(
                    text = "${if (weatherState.value.currentObservation != null) weatherState.value.currentObservation?.atmosphere?.humidity else 0}°",
                    fontWeight = FontWeight.W800,
                    fontSize = 30.sp,
                    color = Color(0x77FFFFFF),
                )

                Spacer(modifier = Modifier.height(24.dp))

            }
        }


        icSunCould.resource.resource?.let {
            Image(
                asset = it,
                modifier = Modifier
                    .size(120.dp)
                    .align(Alignment.BottomEnd)
            )
        }

    }
}

@Composable
fun MiniBody(
    index: Int,
    item: Forecast,
    modifier: Modifier = Modifier
        .padding(start = 12.dp, end = 12.dp, bottom = 24.dp)
        .width(148.dp)
        .height(220.dp)
        .padding(2.dp),
    clickableModifier: Modifier = Modifier,
    shape: Shape = RoundedCornerShape(32.dp),
    textSizeTitle: TextUnit = 16.sp,
    textSizeLowHigh: TextUnit = 32.sp,
    textSizeLow: TextUnit = 15.sp,
    topSpacerHeight: Dp = 18.dp,
    imageSize: Dp = 52.dp,
    showInfo: Boolean = false,
    elevation: Dp = 8.dp
) {
    val icSun = loadVectorResource(id = R.drawable.ic_sun)
    val icLighticons = loadVectorResource(id = R.drawable.ic_lighticons)
    val icLighticons18 = loadVectorResource(id = R.drawable.ic_lighticons_18)
    val icLighticons27 = loadVectorResource(id = R.drawable.ic_lighticons_27)

    val icCloud = loadVectorResource(id = R.drawable.ic_cloud)

    val colors = arrayListOf<Color>(nextDays1, nextDays2, nextDays3, nextDays4, nextDays5, nextDays6)
    val color = colors[index % colors.size]

    Card(
        shape = shape,
        backgroundColor = color,
        modifier = modifier,
        elevation = elevation,
    ) {
        Box(
            modifier = clickableModifier
        ) {

            icCloud.resource.resource?.let {
                Image(
                    asset = it,
                    modifier = Modifier
                        .align(Alignment.BottomEnd)
                        .padding(bottom = 0.dp)
                )
            }

            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier.align(Alignment.TopCenter)
            ) {
                Spacer(modifier = Modifier.height(topSpacerHeight))
                Text(
                    text = item.day,
                    color = Color.White,
                    style = MaterialTheme.typography.h3,
                    fontWeight = FontWeight.W500,
                    fontSize = textSizeTitle,
                    modifier = Modifier
                        .align(Alignment.CenterHorizontally)
                )

                Spacer(modifier = Modifier.height(12.dp))

                val image = when(index % 4) {
                    0 -> icSun
                    1 -> icLighticons
                    2 -> icLighticons18
                    3 -> icLighticons27
                    else -> icSun
                }

                image.resource.resource?.let {
                    Image(
                        asset = it,
                        modifier = Modifier
                            .size(imageSize)
                    )
                }

                Spacer(modifier = Modifier.height(12.dp))

                Text(
                    text = "${(item.low + item.high) / 2}°",
                    fontWeight = FontWeight.W300,
                    fontSize = textSizeLowHigh,
                    color = Color(0xFFFFFFFF),
                )

                Row() {
                    Text(
                        text = "${item.low}°",
                        fontSize = textSizeLow,
                        color = Color(0xAAFFFFFF),
                        modifier = Modifier.padding(end = 8.dp)
                    )

                    Text(
                        text = "${item.high}°",
                        fontWeight = FontWeight.W400,
                        fontSize = textSizeLow,
                        color = Color(0xDDFFFFFF),
                        modifier = Modifier.padding(start = 8.dp)
                    )
                }

                Spacer(modifier = Modifier.height(32.dp))

                if (showInfo) {
                    val icMiniCardSun = loadVectorResource(id = R.drawable.ic_mini_card_sun)
                    val icMiniCard2 = loadVectorResource(id = R.drawable.ic_mini_card_2)
                    val icMiniCard3 = loadVectorResource(id = R.drawable.ic_mini_card_3)
                    val icMiniCard4 = loadVectorResource(id = R.drawable.ic_mini_card_4)
                    val icMiniCard5 = loadVectorResource(id = R.drawable.ic_mini_card_5)
                    val icUmbrella = loadVectorResource(id = R.drawable.ic_umbrella)

                    Box(
                        modifier = Modifier
                            .padding(6.dp)
                    ) {
                        Card(
                            shape = RoundedCornerShape(32.dp),
                            backgroundColor = Color.White,
                            modifier = Modifier
                                .padding(top = 18.dp)
                                .fillMaxWidth().height(185.dp)
                                .padding(24.dp),
                            elevation = 0.dp,
                        ) {
                        }

                        ScrollableRow(
                            modifier = Modifier.align(Alignment.TopCenter)
                        ) {
                            val state = remember { mutableStateOf(-1) }
                            for (i in 0 until 5) {
                                Card(
                                    elevation = if (state.value != i) 0.dp else 4.dp,
                                    backgroundColor = if (state.value  != i) Color.Transparent else Color.White,
                                    shape = RoundedCornerShape(if (state.value != i) 0.dp else 22.dp),
                                    modifier = Modifier
                                        .width(56.dp)
                                        .padding(start = 7.dp, top = 8.dp, bottom = 12.dp, end = 7.dp)
                                        .clickable(onClick = {
                                            if (state.value == i) {
                                                state.value = -1
                                            } else {
                                                state.value = i
                                            }
                                        }, enabled = true)
                                ) {
                                    Column(
                                        horizontalAlignment = Alignment.CenterHorizontally,
                                        modifier = Modifier
                                            .padding(top = 12.dp, bottom = 24.dp)
                                    ) {

                                        if (state.value != i) {
                                            Spacer(modifier = Modifier.size(30.dp))
                                        } else {
                                            icUmbrella.resource.resource?.let {
                                                Image(
                                                    asset = it,
                                                    modifier = Modifier
                                                        .size(30.dp)
                                                )
                                            }
                                        }

                                        Spacer(modifier = Modifier.size(18.dp))
                                        Text(
                                            text = "1${4+i}:00",
                                            fontSize = 14.sp,
                                            color= mainTextColor
                                        )

                                        var image = when(i) {
                                            0 -> icMiniCardSun
                                            1 -> icMiniCard2
                                            2 -> icMiniCard3
                                            3 -> icMiniCard4
                                            4 -> icMiniCard5
                                            else -> icMiniCardSun
                                        }

                                        image.resource.resource?.let {
                                            Image(
                                                asset = it,
                                                modifier = Modifier
                                                    .size(30.dp)
                                            )
                                        }

                                        Text(
                                            text = "${25 - (i + 1)}°",
                                            fontSize = 16.sp,
                                            color= mainTextColor)
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }
    }
}

sealed class MenuAction(
    @StringRes val label: Int,
    @DrawableRes val icon: Int) {

    object Share : MenuAction(R.string.search, R.drawable.ic_search)
}