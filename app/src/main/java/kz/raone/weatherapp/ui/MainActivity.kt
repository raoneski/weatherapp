package kz.raone.weatherapp.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.runtime.*
import androidx.compose.ui.focus.ExperimentalFocus
import androidx.compose.ui.platform.setContent
import androidx.lifecycle.lifecycleScope
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kz.raone.weatherapp.api.pojo.SearchItem
import kz.raone.weatherapp.api.pojo.YahooWeather
import kz.raone.weatherapp.manager.SettingsManager
import kz.raone.weatherapp.presenter.YahooPresenter
import kz.raone.weatherapp.ui.composable.SplashScreen
import kz.raone.weatherapp.ui.composable.WeatherReport
import kz.raone.weatherapp.view.YahooWeatherView
import org.koin.android.ext.android.get

class MainActivity : AppCompatActivity(), YahooWeatherView {
    val yahooPresenter = YahooPresenter()

    val settingsManager: SettingsManager = get()
    val splashState = mutableStateOf(false)
    val weatherState = mutableStateOf<YahooWeather>(YahooWeather())
    val searchItems = mutableStateOf<List<SearchItem>>(listOf())
    var searchItem: String = ""

    val progressBarState = mutableStateOf(false)

    @ExperimentalFocus
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            if (splashState.value) SplashScreen(yahooPresenter)
            else WeatherReport(weatherState, onBackPressedDispatcher, searchItems, yahooPresenter, progressBarState)
        }

        yahooPresenter.onAttach(this)

        lifecycleScope.launch {
            settingsManager.loadBoolean(SettingsManager.DISPLAY_SPLASH_SCREEN).collect{
                splashState.value = it
            }
        }

        GlobalScope.launch {
            settingsManager.loadString(SettingsManager.SEARCH_ITEM).collect {
                val searchItem1 = Gson().fromJson<SearchItem>(it, SearchItem::class.java)
                if (searchItem != it ) {
                    if (searchItem1 != null && searchItem1!!.location != null) {
                        yahooPresenter.getYahooWeather(searchItem1!!.location)
                    } else {
                        yahooPresenter.getYahooWeather()
                    }

                    searchItem = it
                }
            }
        }

        lifecycleScope.launch {
            settingsManager.cachedWeather.collect{
                val yahooWeather = Gson().fromJson<YahooWeather>(it, YahooWeather::class.java)

                if (yahooWeather != null && weatherState.value != yahooWeather) {
                    weatherState.value = yahooWeather

                }
            }
        }

        lifecycleScope.launch {
            settingsManager.loadString(SettingsManager.SEARCH_ITEMS).collect {
                val myType = object : TypeToken<List<SearchItem>>() {}.type
                val searchItemsList = Gson().fromJson<List<SearchItem>>(it, myType)

                if (searchItemsList != null) {
                    if (searchItems.value != searchItemsList) searchItems.value = searchItemsList
                } else {
                    val list = arrayListOf<SearchItem>()

                    list.add(
                        SearchItem(
                            "Almaty",
                            "Kazakhstan",
                            "almaty,kz"
                        )
                    )
                    list.add(
                        SearchItem(
                            "Astana",
                            "Kazakhstan",
                            "astana,kz"
                        )
                    )

                    GlobalScope.launch {
                        settingsManager.saveString(SettingsManager.SEARCH_ITEMS, Gson().toJson(list))
                    }
                }
            }
        }

    }

    override fun showError(message: String) {

    }

    override fun showProgressBar() {
        progressBarState.value = true
    }

    override fun hideProgressBar() {
        progressBarState.value = false
    }

    override fun showYahooWeather(yahooWeather: YahooWeather, location: String) {
        if (yahooWeather.location != null) {
            if (location !in searchItems.value.map { "${it.location}" }) {
                lifecycleScope.launch {
                    val list = searchItems.value as ArrayList<SearchItem>

                    val newItem = SearchItem(
                        yahooWeather.location.city,
                        yahooWeather.location.country,
                        location
                    )

                    list.add(newItem)

                    settingsManager.saveString(SettingsManager.SEARCH_ITEM, Gson().toJson(newItem))
                    settingsManager.saveString(SettingsManager.SEARCH_ITEMS, Gson().toJson(list))
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        yahooPresenter.onDetach()
    }
}
