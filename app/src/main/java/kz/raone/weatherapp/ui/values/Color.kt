package kz.raone.weatherapp.ui.values

import androidx.compose.ui.graphics.Color

val purple50 = Color(0x1D5D50FE)
val purple200 = Color(0xFF5D50FE)
val purple500 = Color(0xFF6200EE)
val purple700 = Color(0xFF3700B3)
val teal200 = Color(0xFF03DAC5)


val mainPurpleColor = Color(0xFF5C2AFF)
val mainTextColor = Color(0xFF130E51)
val secondaryTextColor = Color(0xFF130E51)
val tabTextActiveColor = purple200
val tabBgActiveColor = Color(0x2DADA7FE)

val splashGradientStartColor = purple200
val splashGradientEndColor = Color(0xFF8572FC)

val nextDays1 = Color(0xFF28E0AE)
val nextDays2 = Color(0xFFFF0090)
val nextDays3 = Color(0xFFFFAE00)
val nextDays4 = Color(0xFF0090FF)
val nextDays5 = Color(0xFFDC0000)
val nextDays6 = Color(0xFF0051FF)


val colorClearRed = Color(0xFFFF0090)

