package kz.raone.weatherapp.ui.composable

import android.content.res.Resources
import androidx.compose.foundation.Image
import androidx.compose.foundation.Text
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.FloatingActionButton
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.VerticalGradient
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.loadVectorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.ui.tooling.preview.Preview
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kz.raone.weatherapp.R
import kz.raone.weatherapp.manager.SettingsManager
import kz.raone.weatherapp.presenter.YahooPresenter
import kz.raone.weatherapp.ui.values.mainPurpleColor
import kz.raone.weatherapp.ui.values.splashGradientEndColor
import kz.raone.weatherapp.ui.values.splashGradientStartColor
import org.koin.core.context.GlobalContext

@Composable
fun SplashScreen(yahooPresenter: YahooPresenter) {
    val settingsManager: SettingsManager = GlobalContext.get().koin.get ()
    val image = loadVectorResource(id = R.drawable.ic_splash_bg_2)
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(
                VerticalGradient(listOf(splashGradientStartColor, splashGradientEndColor),
                    0f,
                    1000f)
            )
    ) {
        Column(
            modifier = Modifier
                .align(Alignment.BottomEnd)
        ) {
            image.resource.resource?.let {
                Image(
                    asset = it,
                    modifier = Modifier
                        .fillMaxWidth())
            }
            
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(100.dp)
                    .background(mainPurpleColor))
        }


        Column(
            modifier = Modifier.align(Alignment.TopCenter).padding(48.dp),
        ) {

            Text(
                text = stringResource(id = R.string.app_name),
                fontSize = 30.sp,
                color = colorResource(id = R.color.white),
                style = MaterialTheme.typography.h4
            )
            Text(
                text = stringResource(id = R.string.splash_screen_secondary_text),
                fontSize = 18.sp,
                color = colorResource(id = R.color.white),
                style = MaterialTheme.typography.h2
            )
        }

        FloatingActionButton(
            onClick = {
                GlobalScope.launch {
                    settingsManager.saveBoolean(SettingsManager.DISPLAY_SPLASH_SCREEN, false)
                }

                yahooPresenter.getYahooWeather()
            },
            modifier = Modifier
                .align(Alignment.BottomCenter)
                .padding(bottom = 60.dp)
                .width(200.dp)
                .height(42.dp),
            backgroundColor = Color.White
        ) {
            Text(
                text = stringResource(id = R.string.splash_screen_button_explore).toUpperCase(),
                fontSize = 18.sp,
                fontWeight = FontWeight.W700
            )
        }
    }
}

