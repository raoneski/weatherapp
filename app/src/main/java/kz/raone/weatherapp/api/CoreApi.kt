package kz.raone.weatherapp.api

import android.os.Build
import android.util.Log
import kz.raone.weatherapp.api.pojo.YahooWeather
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import org.apache.commons.lang3.RandomStringUtils
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query
import java.net.URLEncoder
import java.util.*
import java.util.concurrent.TimeUnit
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec


interface CoreApi {
    companion object {
        val requestTimeout = 60L
        const val appId = "cgJdReWm"
        const val consumerKey = "dj0yJmk9bGF3YXdrcnFHbE1NJmQ9WVdrOVkyZEtaRkpsVjIwbWNHbzlNQT09JnM9Y29uc3VtZXJzZWNyZXQmc3Y9MCZ4PWY0"
        const val consumerSecret = "7963721f7081ac9bb10034398327e48e6a52af41"
        const val BASE_URL = "https://weather-ydn-yql.media.yahoo.com"


        val instance by lazy {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY

            val client = OkHttpClient.Builder()
                .hostnameVerifier { hostname, session -> true }
                .addInterceptor(interceptor)
                .connectTimeout(requestTimeout, TimeUnit.SECONDS)
                .writeTimeout(requestTimeout, TimeUnit.SECONDS)
                .readTimeout(requestTimeout, TimeUnit.SECONDS)
                .callTimeout(requestTimeout, TimeUnit.SECONDS)

                .build()

            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            retrofit.create(CoreApi::class.java)
        }



        fun getAuthorizationLine(location: String): String {

            val url = "https://weather-ydn-yql.media.yahoo.com/forecastrss"

            val timestamp = Date().time / 1000
            val nonce = ByteArray(32)
            val rand = Random()
            rand.nextBytes(nonce)
            val oauthNonce: String = RandomStringUtils.random(10, true, true)

            val parameters: MutableList<String> = ArrayList()
            parameters.add("oauth_consumer_key=$consumerKey")
            parameters.add("oauth_nonce=$oauthNonce")
            parameters.add("oauth_signature_method=HMAC-SHA1")
            parameters.add("oauth_timestamp=$timestamp")
            parameters.add("oauth_version=1.0")
            parameters.add("location=" + URLEncoder.encode(location, "UTF-8"))
            parameters.add("format=json")
            Collections.sort(parameters)

            val parametersList = StringBuffer()
            for (i in parameters.indices) {
                parametersList.append((if (i > 0) "&" else "") + parameters[i])
            }

            val signatureString = "GET&" +
                URLEncoder.encode(url, "UTF-8") + "&" +
                URLEncoder.encode(parametersList.toString(), "UTF-8")
            var signature: String? = null
            try {
                val signingKey = SecretKeySpec("$consumerSecret&".toByteArray(), "HmacSHA1")
                val mac = Mac.getInstance("HmacSHA1")
                mac.init(signingKey)
                val rawHMAC = mac.doFinal(signatureString.toByteArray())
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    val encoder = Base64.getEncoder()
                    signature = encoder.encodeToString(rawHMAC)
                }
            } catch (e: Exception) {
                System.err.println("Unable to append signature")
                System.exit(0)
            }

            var authorizationLine = "OAuth " +
                "oauth_consumer_key=\"" + consumerKey.toString() + "\", " +
                "oauth_nonce=\"" + oauthNonce.toString() + "\", " +
                "oauth_timestamp=\"" + timestamp.toString() + "\", " +
                "oauth_signature_method=\"HMAC-SHA1\", " +
                "oauth_signature=\"" + signature.toString() + "\", " +
                "oauth_version=\"1.0\""

            return authorizationLine

        }
    }

    @GET("/forecastrss?format=json")
    suspend fun getYahooWeather(
        @Query("location") location: String,
        @Header("Authorization") a: String = getAuthorizationLine(location),
        @Header("X-Yahoo-App-Id") b: String = appId,
        @Header("Content-Type") c: String = "application/json"
    ): YahooWeather

}