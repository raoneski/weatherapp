package kz.raone.weatherapp.api.pojo

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName

data class YahooWeather(
    val location: Location? = null,

    @SerializedName("current_observation")
    val currentObservation: CurrentObservation? = null,

    val forecasts: List<Forecast> = listOf()
) {

    override fun toString(): String {
        return Gson().toJson(this)
    }
}

data class Location(
    val city: String,
    val region: String,
    val woeid: Int,
    val country: String,
    val lat: Float,
    val long: Float,
    val timezone_id: String,
)

data class CurrentObservation(
    val wind: Wind,
    val atmosphere: Atmosphere,
    val astronomy: Astronomy,
    val condition: Condition,
    val pubDate: Long,
)


data class Wind(
    val chill: Int,
    val direction: Int,
    val speed: Float
)

data class Atmosphere(
    val humidity: Int,
    val visibility: Float,
    val pressure: Float,
    val rising: Int,
)

data class Astronomy(
    val sunrise: String,
    val sunset: String,
)

data class Condition(
    val text: String,
    val code: Int,
    val temperature: Int,
)


data class Forecast(
    val day: String,
    val date: Long,
    val low: Int,
    val high: Int,
    val text: String,
    val code: String,
)

data class SearchItem(
    val city: String,
    val country: String,
    val location: String
)