//package kz.raone.weatherapp.api.pojo
//
//data class Coord(
//    val lon: Float,
//    val lat: Float,
//)
//
//data class Weather(
//    val id: Int,
//    val main: String,
//    val description: String,
//    val icon: String,
//)
//
//data class Main(
//    val temp: Float,
//    val feels_like: Float,
//    val temp_min: Float,
//    val temp_max: Float,
//    val pressure: Float,
//    val humidity: Float,
//)
//
//
//data class Wind(
//    val speed: Int,
//    val deg: Int
//)
//
//data class Clouds(
//    val all: Int,
//)
//
//data class Sys(
//    val type: Int,
//    val id: Int,
//    val country: String,
//    val sunrise: Long,
//    val sunset: Long,
//)
//
//
//data class WeatherResponse(
//    val coord: Coord,
//    val weather: Weather,
//    val base: String,
//    val main: Main,
//    val visibility: Int,
//    val wind: Wind,
//    val clouds: Clouds,
//    val dt: Long,
//    val sys: Sys,
//    val timezone: Int,
//    val id: Long,
//    val name: String,
//    val code: Int
//)