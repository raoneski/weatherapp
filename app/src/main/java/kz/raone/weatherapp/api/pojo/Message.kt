package kz.raone.weatherapp.api.pojo

import com.google.gson.Gson

data class Message(
    val messageText: String = ""
) {

    override fun toString(): String {
        return Gson().toJson(this)
    }
}