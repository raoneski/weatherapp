package kz.raone.weatherapp.view

import kz.raone.weatherapp.api.pojo.YahooWeather

interface YahooWeatherView {
    fun showError(message: String)
    fun showProgressBar()
    fun hideProgressBar()

    fun showYahooWeather(yahooWeather: YahooWeather, location: String)
}