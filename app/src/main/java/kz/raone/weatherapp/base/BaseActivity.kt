package kz.raone.weatherapp.base

import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity

open class BaseActivity: AppCompatActivity() {
    var disposable = CompositeJob()

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        disposable = CompositeJob()
        super.onCreate(savedInstanceState, persistentState)
    }

    override fun onDestroy() {
        disposable.dispose()
        super.onDestroy()
    }
}

abstract class BaseActivityAbstract