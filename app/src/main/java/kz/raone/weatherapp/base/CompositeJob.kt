package kz.raone.weatherapp.base

import kotlinx.coroutines.Job

class CompositeJob {

    private val map = hashMapOf<String, Job>()

    fun add(job: Job, key: String = job.hashCode().toString())
            = map.put(key, job)?.cancel()

    fun dispose(key: String) = map[key]?.cancel()

    fun dispose() = map.keys.forEach {key -> map[key]?.cancel() }

    fun size() = map.size
}