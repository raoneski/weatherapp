package kz.raone.weatherapp.base

import android.util.Log

abstract class BasePresenter<T> : Presenter() {
    var mainView: T? = null
    var disposable = CompositeJob()

    open fun onAttach(view: T) {
        mainView = view
        disposable = CompositeJob()
    }

    open fun onDetach(){
        mainView = null
        disposable.dispose()
    }
}

abstract class Presenter

