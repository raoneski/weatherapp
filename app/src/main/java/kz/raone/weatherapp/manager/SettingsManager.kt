package kz.raone.weatherapp.manager

import android.content.SharedPreferences
import androidx.datastore.DataStore
import androidx.datastore.preferences.Preferences
import androidx.datastore.preferences.edit
import androidx.datastore.preferences.preferencesKey
import androidx.lifecycle.asLiveData
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map

class SettingsManager(val dataStore: DataStore<Preferences>)  {
    val cachedWeather = loadString(CACHED_WEATHER)

    suspend fun saveInt(key: String, value: Int) {
        dataStore.edit {
            it[preferencesKey(key)] = value
        }
    }

    suspend fun loadInt(key: String): Flow<Int> {
        return dataStore.data.map { it[preferencesKey(key)] ?: 0 }
    }

    suspend fun saveFloat(key: String, value: Float) {
        dataStore.edit {
            it[preferencesKey(key)] = value
        }
    }

    suspend fun loadFloat(key: String): Flow<Float> {
        return dataStore.data.map { it[preferencesKey(key)] ?: 0.0f }
    }

    suspend fun saveBoolean(key: String, value: Boolean) {
        dataStore.edit {
            it[preferencesKey(key)] = value
        }
    }

    fun loadBoolean(key: String): Flow<Boolean> {
        when (key) {
            DISPLAY_SPLASH_SCREEN ->{
                return dataStore.data.map { it[preferencesKey(key)] ?: true }
            }
        }

        return dataStore.data.map { it[preferencesKey(key)] ?: false }
    }

    suspend fun saveLong(key: String, value: Long) {
        dataStore.edit {
            it[preferencesKey(key)] = value
        }
    }

    suspend fun loadLong(key: String): Flow<Long> {
        return dataStore.data.map { it[preferencesKey(key)] ?: 0L }
    }

    suspend fun saveString(key: String, value: String) {
        dataStore.edit {
            it[preferencesKey(key)] = value
        }
    }

    fun loadString(key: String): Flow<String> {
        return dataStore.data.map { it[preferencesKey(key)] ?: "" }
    }


    companion object {
        const val CACHED_WEATHER = "trfXkkex3gwkyAsP5hP5M6"
        const val SEARCH_ITEMS = "t2spF4dG26thEqqa8trg4ct"
        const val SEARCH_ITEM = "FVagp9CvdnbnANGSwCNf7j"
        const val DISPLAY_SPLASH_SCREEN = "zGskpVgz7ehWTB6GswY4pd"
    }
}

// Keys.
//8Y2VE4MGgLTz9PwFXd4hXf
//vJ9XG2ubEeBp8DnAjTJuUc
//F6tebEMvjxZcjj3fRa5ufn
//dJW6FXYcBPF5HU9sX8dB8C
//GCQ8vE9PLNqbuntJ9m7GuR
//cvxdx48cvnmnNTG6GwXGJE
//WaKThQjHC58aXRgdqH2CAg
//ZypmYtJTMzWk56XErVEkEt
//u7GwKjDqxjQWVrwn2YVYpG
//8aVBtyzHUMuJM3K7wDVFBJ
//pTXqEVGRgvVuHpuxVXZMdW
//Q58ef3ghFyYsN3bccm7k8D
//n59RMJSh8YsB7HX7udbMLn
//jYaLzbQmwZCu6dgPvUcKCP
//efvG3rfRQbtMYEDkk7wuy8
//n77rUPTLmUsdvHUzmSzULL