package kz.raone.weatherapp.di

import androidx.datastore.DataStore
import androidx.datastore.preferences.Preferences
import androidx.datastore.preferences.createDataStore
import kz.raone.weatherapp.interactor.YahooInteractorImpl
import kz.raone.weatherapp.manager.SettingsManager
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val applicationModule = module (override = true) {
    single<DataStore<Preferences>> { androidContext().createDataStore(name = "dwa#UEKBjLsG7%&wmqDDwjv%@dwaDW^m68p-^qps?zE*7mh6&DW%^N&aYJJ:r1") }
    single<SettingsManager> { SettingsManager(get()) }

    single { YahooInteractorImpl() }
}