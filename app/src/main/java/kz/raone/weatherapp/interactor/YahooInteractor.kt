package kz.raone.weatherapp.interactor

import kz.raone.weatherapp.api.CoreApi
import kz.raone.weatherapp.api.pojo.YahooWeather

class YahooInteractorImpl(): YahooInteractor {

    override suspend fun getYahooWeather(location: String): YahooWeather {
        return CoreApi.instance.getYahooWeather(location)
    }
}

interface YahooInteractor {
    suspend fun getYahooWeather(location: String): YahooWeather
}
