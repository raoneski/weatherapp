package kz.raone.weatherapp

import android.content.Context
import android.text.Editable
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.unit.Dp
import kz.raone.weatherapp.util.convertLongToTime

fun String?.isNotNullOrEmpty(): Boolean {
    return this != null && this.isNotEmpty()
}

fun String.toEditable(): Editable =  Editable.Factory.getInstance().newEditable(this)


fun View.hideKeyboard() {
    val imm = this.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(this.windowToken, 0)
}

fun saveLogs(message: String, from: String) {
    Log.i("mainLogs", "$from: $message")
}

fun Long.toSDF(): String {
    return convertLongToTime(this)
}

fun String.replaceOperators() = this.replace("--", "+").replace("-+", "-").replace("++", "+").replace("+-", "-")