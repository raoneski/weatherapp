package kz.raone.weatherapp.presenter

import android.util.Log
import kz.raone.weatherapp.base.BasePresenter
import kz.raone.weatherapp.interactor.YahooInteractorImpl
import kotlinx.coroutines.*
import kz.raone.weatherapp.manager.SettingsManager
import kz.raone.weatherapp.view.YahooWeatherView
import org.koin.core.context.GlobalContext
import java.lang.Exception

class YahooPresenter(): BasePresenter<YahooWeatherView>() {
    val interactorImpl = YahooInteractorImpl()
    val settingsManager: SettingsManager = GlobalContext.get().koin.get ()

    fun getYahooWeather(location: String = "almaty,kz") {
        mainView?.showProgressBar()

        disposable.add(
            GlobalScope.launch {
                try {
                    val yahooWeather = interactorImpl.getYahooWeather(location)
                    settingsManager.saveString(SettingsManager.CACHED_WEATHER, "$yahooWeather")

                    GlobalScope.launch(Dispatchers.Main) {
                        if(yahooWeather != null)
                            mainView?.showYahooWeather(yahooWeather, location)

                        mainView?.hideProgressBar()
                    }
                } catch (e: Exception) {
                    GlobalScope.launch(Dispatchers.Main) {
                        mainView?.hideProgressBar()
                    }
                }
            }
        )
    }
}